#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

#include "lst_elm.h"
#include "lst.h"

struct lst_t * new_lst()
{
	struct lst_t * L = (struct lst_t *)calloc(1,sizeof(struct lst_t));
	assert(L);
	return L;
}

void del_lst(struct lst_t *ptrL,void(*ptrFct)()) 
{
	struct lst_elm_t *E=ptrL->head;
	while(E!=NULL)
	{
		struct lst_elm_t *suc=E->suc;
		(*ptrFct)(E);
		E = suc;
	}
	free(ptrL);
	ptrL=NULL;
}

bool empty_lst(const struct lst_t *L)
{
	assert(L);
	return L->numelm == 0;
}

void cons(struct lst_t *L,void *datum)
{
	struct lst_elm_t *elm =new_lst_elm(datum);
	if (L->numelm==0)
	{
		L->tail=elm;
	}
	elm->suc=L->head;
	L->numelm++;
	L->head=elm;
}

void queue(struct lst_t *L,void *datum)
{
	struct lst_elm_t *elm =new_lst_elm(datum);
	if (L->numelm==0)
	{
		L->head=elm;
	}
	else
	{
		L->tail->suc=elm;
	}
	L->numelm++;
	L->tail=elm;
}	

struct lst_elm_t *getHead(struct lst_t *L)
{
	return L->head;
}

struct lst_elm_t *getTail(struct lst_t *L)
{
	return L->tail;
}

int getNumelm(struct lst_t *L)
{
	return L->numelm;
}

void setNumelm(struct lst_t *L,int numElm)
{
	L->numelm=numElm;
}

void print_lst(struct lst_t *L,void(*ptrFct)())
{
	printf("[ ");
	for(struct lst_elm_t *E=L->head;E!=NULL;E=E->suc)
	{
		(*ptrFct)(E->datum);
	}
	printf("]\n\n");
}

struct lst_t *copy_lst(struct lst_t *L)
{
	struct lst_t *copyL=new_lst();
	for(struct lst_elm_t *E=L->head;E!=NULL;E=E->suc)
	{
		queue(copyL,E->datum);
	}
	return copyL;
}