#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

#include "global.h"
#include "objects.h"
#include "dp_array.h"
#include "space_array.h"

void free_states_array(struct states_array_t **states)
{
    assert(states && *states);
    free((*states)->CHM);
    free((*states)->OPT);
    free(*states);
    *states=NULL;
}

void init_opt_chm(struct states_array_t *states)// Private Function
{
    states->OPT=(state_t *)calloc((states->num_obj+1)*(states->Vmax+1),sizeof(state_t));
    states->CHM=(state_t *)calloc((states->num_obj+1)*(states->Vmax+1),sizeof(state_t));
    for(int obj=1;obj<=states->num_obj;obj++)
	{
        for(int bag=0;bag<=states->Vmax;bag++)
		{
            int idx=(states->Vmax+1)*obj+bag;
            states->OPT[idx]=UNDTR;
            states->CHM[idx]=UNDTR;
        }
    }
    for(int bag=0;bag<=states->Vmax;bag++)
	{
        states->CHM[bag]=UNDTR;
    }
}

struct states_array_t *new_states_array(int num_objects,int Vmax)
{
	struct states_array_t *NS=(struct states_array_t *)calloc(1,sizeof(struct states_array_t));
	assert(NS!=NULL);
	NS->num_obj=num_objects;
	NS->Vmax=Vmax;
	init_opt_chm(NS);
	return NS;
}

void push_object_in_array(struct states_array_t *S,struct objects_t *LO,int i)
{
    // Faites attention que les objets dans LO sont rangés à partir de 0
    // tandis qu'ils sont rangés à partue de 1 dans OPT (et CHM)
      for (int bag=0;bag<(S->Vmax+1);bag++)
    {    
        int pred=(i-1)*(S->Vmax+1)+bag;
        int curr=(i)*(S->Vmax+1)+bag;
        int OPT1=S->OPT[pred];

        S->CHM[curr] = INFTY;                  
        if(LO->objects[i-1].volume<=bag)
        {
            int pred_without_i=pred-LO->objects[i-1].volume;
            int OPT2=S->OPT[pred_without_i]+LO->objects[i-1].utility;
            if (OPT2>OPT1)
            {
                S->OPT[curr]=OPT2;
                S->CHM[curr]=bag-LO->objects[i-1].volume;
            }
            else S->OPT[curr]=OPT1;        
        }
        else S->OPT[curr]=OPT1;
    }
}

void view_path_array(struct states_array_t * states, struct objects_t * set)
{
    int obj = states->num_obj;
    int vol = states->Vmax;
    int idx = obj * (states->Vmax + 1) + vol;
    bool nonstop = (obj == 0);
    printf("*********\nTotal packaging utility : %d\n*********\n", states->OPT[idx]);
    while(!nonstop)
    {
        if(states->CHM[idx] != INFTY)
            { // object actually put in bag
                printf("\tobjet #%d(%d, %d)\n", obj, set->objects[obj-1].volume, set->objects[obj-1].utility);
                nonstop = (states->CHM[idx] == 0);
                vol = states->CHM[idx];
            }
            obj -= 1;
            nonstop = nonstop || (obj == 0);
            idx = obj * (states->Vmax + 1) + vol;
    }
    printf("\n");
}

void view_opt(struct states_array_t * states)
{
  	printf("OPT |\t");
	for(int bag = 0; bag < (states->Vmax + 1); bag += 1) printf("%2d\t", bag);
	printf("\n----|");
	for(int bag = 0; bag < (states->Vmax + 1); bag += 1) printf("--------");
	printf("\n");
	for(int obj = 0; obj < (states->num_obj + 1); obj += 1)
    {
		printf("%3d |\t", obj);
		for(int bag = 0; bag < (states->Vmax + 1); bag += 1)
        {
			int idx = obj * (states->Vmax + 1) + bag;
			if(states->OPT[idx] == INFTY) printf("INF\t");
			else if(states->OPT[idx] == UNDTR) printf("UND\t");
			else printf("%2d\t", states->OPT[idx]);
		}
		printf("\n");
	}
	printf("\n");
}

void view_chm(struct states_array_t * states) {
  printf("CHM |\t");
	for(int bag = 0; bag < (states->Vmax + 1); bag += 1) printf("%2d\t", bag);
	printf("\n----|");
	for(int bag = 0; bag < (states->Vmax + 1); bag += 1) printf("--------");
	printf("\n");
	for(int obj = 0; obj < (states->num_obj + 1); obj += 1)
    {
		printf("%3d |\t", obj);
		for(int bag = 0; bag < (states->Vmax + 1); bag += 1)
        {
			int idx = obj * (states->Vmax + 1) + bag;
			if(states->CHM[idx] == INFTY) printf("PRE\t");
			else if(states->CHM[idx] == UNDTR) printf("UND\t");
			else printf("%2d\t", states->CHM[idx]);
		}
		printf("\n");
	}
	printf("\n");
}