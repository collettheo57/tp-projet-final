#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

#include "lst_elm.h"
#include "lst.h"
#include "global.h"
#include "objects.h"
#include "prec.h"

struct retained_t *new_bag()
{
    struct retained_t *bagpack=(struct retained_t *)calloc(1,sizeof(struct retained_t));
    bagpack->objects_list=new_lst();
    return bagpack;
}

void copy_bag(struct retained_t *duplicata,struct retained_t *bagpack)
{
    duplicata->objects_list=copy_lst(bagpack->objects_list);
    duplicata->utilities_sum=bagpack->utilities_sum;
}

void free_bag(struct retained_t *bagpack)
{
    del_lst(bagpack->objects_list,&rm_object);
    free(bagpack);
    bagpack=NULL;
}

void clean_bag(struct retained_t *bagpack)
{
    del_lst(bagpack->objects_list,&rm_object);
    bagpack->objects_list=new_lst();
    assert(bagpack->objects_list);
    bagpack->utilities_sum=0;
}

void push_object_in_bag(struct retained_t *bagpack, struct object_t *ptr_object)
{
    cons(bagpack->objects_list,ptr_object);
    bagpack->utilities_sum+=ptr_object->utility;
}

void view_bagpack(struct retained_t *bagpack, const char *title)
{
	void(*ptr_view_fct)(struct object_t *)=&view_object;
	printf("\n*****************\nVIEW BAGPACKAGING\t%s\n",title);
	print_lst(bagpack->objects_list,ptr_view_fct);
	printf("\t\tWith utilities sum = %d\n\n",bagpack->utilities_sum);
}

void prec(const int Vmax, struct objects_t *obj_set,struct retained_t *bag)
{
    const int nb_objects=obj_set->nb_objects;
    struct retained_t *duplicata=new_bag();
    copy_bag(duplicata,bag);
    struct retained_t *best_bagpack=new_bag();
    copy_bag(best_bagpack,bag);// Pred: best bag is bag

    for(int obj_idx=obj_set->first_idx;obj_idx<nb_objects;obj_idx++) 
    {// Verif: Try new objects
        struct object_t *ptr_object=obj_set->objects + obj_idx;
        int curr_volume = Vmax-ptr_object->volume;
        if(curr_volume>=0)
        {
            copy_bag(duplicata,bag);
            push_object_in_bag(duplicata,ptr_object);
            obj_set->first_idx++;
            prec(curr_volume,obj_set,duplicata);
            if(duplicata->utilities_sum>best_bagpack->utilities_sum)
            {
                clean_bag(best_bagpack);
                copy_bag(best_bagpack,duplicata);
            }
        }
    }
    clean_bag(bag);
    copy_bag(bag,best_bagpack);
    free_bag(best_bagpack);
    free_bag(duplicata);
}
