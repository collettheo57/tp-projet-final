#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

#include "global.h"
#include "objects.h"
#include "dp_array.h"
#include "space_array.h"


void dp_array(bool utility,int Vmax,struct objects_t *objects)
{
    struct states_array_t *states=new_states_array(objects->nb_objects,Vmax);
    assert(states!=NULL);
    #ifdef _TRACE_
        view_opt(states);
    #endif

    for(int i = 1; i <= objects->nb_objects; i++) {
        #ifdef _TRACE_
            printf("#%d Object\n", i);
        #endif
        push_object_in_array(states, objects, i);
        #ifdef _TRACE_
            view_opt(states);
            view_chm(states);
        #endif
    }
    view_path_array(states, objects);
    free_states_array(&states);
}
