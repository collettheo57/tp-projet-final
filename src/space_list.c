#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

#include "lst_elm.h"
#include "lst.h"
#include "global.h"
#include "objects.h"
#include "dp_list.h"
#include "space_list.h"

// Essais non concluants

states_t *new_states_list()
{
    states_t *S=(states_t *)calloc(1,sizeof(states_t));
}

struct dyn_state_t *new_dyn_state_t()
{
    struct dyn_state_t *D=(struct dyn_state_t *)calloc(1,sizeof(struct dyn_state_t));
}

void push_state(states_t *ptr_states,struct dyn_state_t *S)
{
    
}

void free_states_list(states_t *states,void (*ptr_fct)())
{

}

void free_state(struct dyn_state_t *S)
{

}



void view_states_list(states_t *states);

states_t *cpy_states_list(states_t *ptr_states);

struct dyn_state_t *push_object(const struct object_t *O,struct dyn_state_t *S);
void view_state(const struct dyn_state_t *S);
void view_best_bagpack(struct dyn_state_t *S);

struct dyn_state_t *best_bagpack(const states_t *states);
