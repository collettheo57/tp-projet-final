#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

#include "global.h"
#include "objects.h"

struct objects_t *new_objects(int argc, char ** argv, bool utility)
{
    struct objects_t *set=(struct objects_t *)calloc(1,sizeof(struct objects_t));
    int i,j, offset = 4;
    set->nb_objects = (utility) ? (argc - offset) / 2 : (argc - offset);
    set->objects =(struct object_t *)calloc(set->nb_objects,sizeof(struct object_t));
    if(utility)
    {
        
        for(i=offset,j=0;i<argc;i+=2,j++)
        {
        	set->objects[j].volume=atoi(argv[i]);
            set->objects[j].utility=atoi(argv[i+1]);
        }
    } 
    else
    {
        for(i=offset,j=0;i<argc;i++,j++)
        {
        	set->objects[j].volume=atoi(argv[i]);
            set->objects[j].utility=atoi(argv[i]);
        }
    }
    set->first_idx=0;
    return set;
}

void view_object(struct object_t * object)
{
    printf("(%d , %d) ", object->volume, object->utility);
}

void view_objet_set(struct objects_t * set)
{
	printf("\n*** View objet set ***\n");
	for(int o = 0; o < set->nb_objects;o++)
    {
        view_object(set->objects + o);
    }
	printf("\n**********************\n");
}

void rm_object(struct object_t *object)
{
    free(object);
    object=NULL;
}