Projet Final de Programmation avancée de Théo Collet [collettheo57@gmail.com]
    Programme disponible sur GitLab à l'adresse : https://gitlab.com/collettheo57/tp-projet-final

Ce projet à pour but la réalisation de 3 sous modules qui selon un set d'objets (composés d'un volume et d'une utilité chacun) rentré par l'utilisateur doivent renvoyer quels objects doivent être choisi pour avoir la plus haute utilité avec une contrainte d'un volume maximale totale.
Ces 3 modules sont de différentes programmations :
    - une programmation récursive (prec)
    - une programmation dynamique à mémoire statique (dp_array) 
    - une programmation dynamique à mémoire dynamique (dp_list) (NON FONCTIONNEL)

Pour compiler le programme :

    dans un terminale, se rendre à la racine du dossier "Projet final" dans lequel se trouve ce README
    taper dans le terminale "make"
    ainsi le projet sera compilé dans le sous dossier "bin"

Pour éxecuter le programme :

    dans un terminale, se rendre dans le sous-dossier bin du dossier "Projet final" dans lequel se trouve ce README
    taper dans le terminale la commande "./listen" avec les arguments correspondante à vos attentes et paramètres.

        Le premier agument choisit le type de programmation à executer :
        - "R" pour exécution en récursif
        - "A" pour execution en dynamique à mémoire statique
        - "L" pour exécution en dynamique à mémoire dynamique (NON FONCTIONNEL)

        Le second argument indique si les objets qui vont etre passé en arguments possèdent un volume identique à leur utilité ou non.
        - "1" si les objets possèdent une utilité différentes de leur volume
        - "0" si non

        Le troisième argument détermine le volume maximale totale à ne pas dépasser.

        Les arguments suivants sont les objets, il peut y en avoir autant que besoin.
        - Si le second argument = 1, le volume de l'objet doit être entré en premier puis son utilité après. 2 arguments = 1 obj
        - Si le second argument = 0, chaque valeur représentera un object possédant le même volume et la même utilité. 1 argument = 1 obj

Différents jeux d'essai :
 
./listen R 1 10                                         0 objet                                 res :   u=0
./listen R 1 10 11 3                                    1 objet (11,3)                          res :   u=0

./listen R 1 10 7 11 6 8 4 5                            3 objets (7,11) (6,8) (4,5)             res :(6,8) (4,5) u=13
./listen R 1 10 7 11 6 8 4 5 3 11                       4 objets (7,11) (6,8) (4,5) (3,11)      res :(3,11) (7,11) u=22

./listen R 0 10 7 6 4                                   3 objets (7,7) (6,6) (4,4)              res :(4,4) (6,6) u=10

./listen A 1 10                                         0 objet                                 res :   u=0
./listen A 1 10 11 3                                    1 objet (11,3)                          res :   u=0

./listen A 1 10 7 11 6 8 4 5                            3 objets (7,11) (6,8) (4,5)             res :(6,8) (4,5) u=13
./listen A 1 10 7 11 6 8 4 5 3 11                       4 objets (7,11) (6,8) (4,5) (3,11)      res :(3,11) (7,11) u=22

./listen A 0 10 7 6 4                                   3 objets (7,7) (6,6) (4,4)              res :(4,4) (6,6) u=10



/!\ /!\ /!\ LIMITE /!\ /!\ /!\

Si plusieurs solutions sont possibles pour une utilité max, les modules choisiront le résultats selon l'ordres des objets et non selon le volume total minimal des solutions.  