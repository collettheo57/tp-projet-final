#ifndef _OBJECTS_
#define _OBJECTS_

#include <stdbool.h>

struct object_t 
{
	int volume;
	int utility;
};

struct objects_t
{
	struct object_t *objects;
	int nb_objects;
	int first_idx;
};

// Fonction de création d'un ensemble d'object à partir des arguments entrés
struct objects_t * new_objects(int argc, char ** argv, bool utility);
// Procédure d'affichage d'un objet
void view_object(struct object_t * object);
// Procédure d'affichage d'un ensemble d'objets 
void view_objet_set(struct objects_t * set);
// Procédure de suppression d'un object
void rm_object(struct object_t *object);

#endif