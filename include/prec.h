#ifndef _PREC_
#define _PREC_

#include "objects.h"

struct retained_t
{
	struct lst_t *objects_list;
	int utilities_sum;
};

// Fonction principale de la programmation récursive
void prec(const int Vmax,struct objects_t *object_set,struct retained_t *bagpack);
// Fonction de création d'un sac
struct retained_t *new_bag();
// Procédure de copie d'un sac dans un autre
void copy_bag(struct retained_t *newbagpack, struct retained_t *bagpack);
// Procédure de libération d'un sac
void free_bag(struct retained_t *bagpack);
// Procédure de nettoyage du contenu d'un sac
void clean_bag(struct retained_t *bagpack);
// Procédure d'ajout d'un objet dans un sac
void push_object_in_bag(struct retained_t *bagpack,struct object_t *ptr_object);
// Procédure d'affichage d'un sac avec un titre
void view_bagpack(struct retained_t *bagpack,const char *title);

#endif
