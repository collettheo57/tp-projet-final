#ifndef _DP_ARRAY_
#define _DP_ARRAY_

// Fonction principale de la programmation dynamique à mémoire statique
void dp_array(bool utility,int Vmax,struct objects_t *objects);

#endif