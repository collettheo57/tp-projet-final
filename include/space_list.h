#ifndef _SPACE_LIST_
#define _SPACE_LIST_

#include <stdbool.h>
#include "global.h"
#include "lst.h"

struct dyn_state_t 
{
  struct lst_t *object_list;
  int volume;
  int utility;
};

typedef struct lst_t states_t;


states_t *new_states_list();
void view_states_list(states_t *states);
void free_states_list(states_t *states,void (*ptr_fct)());
states_t *cpy_states_list(states_t *ptr_states);

struct dyn_state_t *new_dyn_state_t();
struct dyn_state_t *push_object(const struct object_t *O,struct dyn_state_t *S);
void view_state(const struct dyn_state_t *S);
void view_best_bagpack(struct dyn_state_t *S);

void push_state(states_t *ptr_states,struct dyn_state_t *S);
struct dyn_state_t *best_bagpack(const states_t *states);
void free_state(struct dyn_state_t *S);

#endif